package com.example.administrador.aula08repeticaoforwhile;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textoResultado;

    String aStooges[] = {"Larry", "Moe", "Curly","Larry", "Moe", "Curly","Larry", "Moe", "Curly"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textoResultado = (TextView) findViewById(R.id.textoResultado);
        textoResultado.setText("");

    }

    public void clicaFor(View v){
        textoResultado.setText("");
        textoResultado.setTextColor(Color.GREEN);
        for(int i =0;i<=50;i++){
            String stringint = Integer.toString(i);
            textoResultado.append("For 0 a "+ stringint + "\n");
            textoResultado.refreshDrawableState();
        }
    }



    public void clicaLimpa(View v){
        textoResultado.setText("");
    }

    public void  clicaWhile2(View v){
        textoResultado.setText("");

        textoResultado.setTextColor(Color.YELLOW);

        int j=0;
        while(j<=50) {
            String stringint1 = Integer.toString(j);
            textoResultado.append("Nome posicao " + stringint1 + "\n");
            j++;
        }

    }
    public void  clicaWhile(View v){
        textoResultado.setText("");

        textoResultado.setTextColor(Color.YELLOW);

        int j=0;
        while(j<aStooges.length) {
            String stringint1 = Integer.toString(j);
            textoResultado.append("Nome posicao " + aStooges[j] + "\n");
            j++;
        }

    }

}
