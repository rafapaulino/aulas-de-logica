package com.example.administrador.aula02botoesacoes;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import android.app.AlertDialog;
import android.widget.RelativeLayout;
import android.graphics.Color;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btAlerta = (Button) findViewById(R.id.botaoAlerta);
        Button btCor = (Button) findViewById(R.id.botaoCor);
        Button btNext = (Button) findViewById(R.id.botaoNext);

        final RelativeLayout fundoTela = (RelativeLayout) findViewById(R.id.minhaView);

        btAlerta.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v){
                alertaNormal(v);
            }
        });

        btCor.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v){
                fundoTela.setBackgroundColor(Color.parseColor("#0B9AE2"));
            }
        });

        btNext.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v){
                setContentView(R.layout.segundatela);
            }
        });
    }

    public void clicaLog(View objetoClicado)
    {
        Log.d("meuLog", "Mensagem do log!!!");
    }

    public void alertaNormal(View view){
        AlertDialog alertDialog;
        alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Funcionou");
        alertDialog.setMessage("Botão Alerta 1");
        alertDialog.show();

    }

}
