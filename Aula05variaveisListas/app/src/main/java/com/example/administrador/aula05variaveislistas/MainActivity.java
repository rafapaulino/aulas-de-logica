package com.example.administrador.aula05variaveislistas;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {


    boolean estado = false;
    int numerointeiro = 33;
    float numeroReal = 1.75f;
    String palavra = "Tito Petri";

    int aNums[] = { 2, 4, 6 };
    String aStooges[] = {"Larry", "Moe", "Curly"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        System.out.println(numerointeiro);
        System.out.println(numeroReal);
        System.out.println(palavra);

        System.out.println(aNums[0]);
        System.out.println(aStooges[0]);

    }


}
