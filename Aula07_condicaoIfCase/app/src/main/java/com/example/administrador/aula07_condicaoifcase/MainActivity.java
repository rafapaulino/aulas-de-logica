package com.example.administrador.aula07_condicaoifcase;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View;

public class MainActivity extends AppCompatActivity {


    private EditText campoNumero;
    private EditText campoSenha;

    private TextView textoNumero;
    private TextView textoSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        campoNumero = (EditText)findViewById(R.id.campoNumero);
        campoSenha = (EditText)findViewById(R.id.campoSenha);

        textoNumero = (TextView)findViewById(R.id.campoNumero);
        textoSenha = (TextView)findViewById(R.id.campoSenha);


    }

    public void clicaNumero(View v){

        String valorNumero = campoNumero.getText().toString();
        int numerointeiro = Integer.parseInt(valorNumero);

        if(numerointeiro%2==0)
        {
            Log.d("meuLog", "Numero PAR!!!");
        }
        else
        {
            Log.d("meuLog", "Numero IMPAR!!!");
        }
    }


    public void clicaSenha(View v){

        String palavraSenha = campoSenha.getText().toString();

        switch(palavraSenha){
            case "Android":
                Log.d("meuLog","Senha Correta!!!");
                break;
            case "ANDROID":
                Log.d("meuLog", "Senha Correta!!!");
                break;
            case "android":
                Log.d("meuLog", "Senha Correta!!!");
                break;
            default:
                Log.d("meuLog", "Senha Errada X");
                break;
        }
    }



}
