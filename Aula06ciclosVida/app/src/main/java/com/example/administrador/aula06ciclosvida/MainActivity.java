package com.example.administrador.aula06ciclosvida;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Log.d("meuLog","Evento Abriu Cena!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("meuLog", "Evento Saindo do App!!!!!!!!!!!!!!!!!!!!!!!!");
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d("meuLog","Evento Tocou na Cena!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        return super.onTouchEvent(event);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("meuLog", "Evento Abriu App!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    }

    @Override
    protected void onPause() {
        super.onPause();Log.d("meuLog", "Evento PAUSOU O APP!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    }
    @Override
    protected void onResume() {
        super.onResume(); Log.d("meuLog", "Evento VOLTOU PARA O APP!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    }
     
    @Override
    protected void onRestart() {
        super.onRestart();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
